package francois.rabanel.tp6;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends AppCompatActivity {
    private final static String TAG = "MainActivity";
    public static String DEFAULT_ENDPOINT = "http://10.0.2.2:8999/ds2438/";
    private final static int CHECKOUT_INTERVAL = 1000;
    Button startButton, stopButton, okButton;
    EditText endpointTxt;
    TextView responseHumidity;
    ProgressBar progressBar;
    Timer timer;
    float result = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Recupérer les différents éléments
        responseHumidity = (TextView) findViewById(R.id.capteur_textview);
        startButton = (Button) findViewById(R.id.start_button);
        stopButton = (Button) findViewById(R.id.stop_button);
        okButton = (Button) findViewById(R.id.valid_button);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        endpointTxt = (EditText) findViewById(R.id.endpoint);

        // Start action
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Start action on:" + DEFAULT_ENDPOINT);
                startButton.setEnabled(false);
                stopButton.setEnabled(true);

                // On crée un Timer qui prendre en argument une TimerTask
                // Le timer sera configuré pour executer toutes les secondes la TimerTask.
                timer = new Timer();
                TimerTask timerTask = new TimerTask() {
                    @Override
                    public void run() {
                        float humidityResponse = pullHumidityAPI(DEFAULT_ENDPOINT);
                        updateInterface(humidityResponse);
                    }
                };
                timer.schedule(timerTask, 0, CHECKOUT_INTERVAL);
            }
        });

        // Stop action
        stopButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Stop action");
                stopTimer();
                resetInterface();
            }
        });

        endpointTxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d(TAG, "Stop action");
                stopTimer();
                resetInterface();
            }
        });

        // Changer endpoint.
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Si on rentre une nouvelle adresse dans le champs editText, on récupère la valeur.
                String endpoint = endpointTxt.getText().toString();
                if (isValidURL(endpoint)) {
                    Log.d(TAG, "Valid endpoint: " + endpoint);
                    DEFAULT_ENDPOINT = endpoint;
                    Toast.makeText(
                            MainActivity.this,
                            "Nouvelle URL enregistrée, vous pouvez cliquer sur Start.",
                            Toast.LENGTH_LONG
                    ).show();
                } else {
                    Log.d(TAG, "Invalid endpoint: " + endpoint);
                    Toast.makeText(MainActivity.this, "URL invalide", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void updateInterface(float value){
        // On a besoin d'utiliser runOnUiThread afin de passer des données
        // Entre notre Thread et l'UIThread qui se chargera de mettre à jour
        // notre vue.
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                responseHumidity.setText("Humidité: " + result + "%");
                progressBar.setProgress(Math.round(result));
            }
        });
    }

    private void stopTimer() {
        if (timer != null) {
            Log.d(TAG, "Timer canceling...");
            timer.cancel();
        } else {
            Log.d(TAG, "Timer does not exist.");
        }
    }
    private void resetInterface() {
        progressBar.setProgress(0);
        responseHumidity.setText(R.string.starting_value_textview);
        stopButton.setEnabled(false);
        startButton.setEnabled(true);
    }

    public boolean isValidURL(String value) {
        try {
            URL uri = new URL(value);
            return true;
        }
        catch (MalformedURLException e) {
            return false;
        }
    }

    private float pullHumidityAPI(String endpoint){
        // On récupère la donnée sur notre serveur API
        try {
            HTTPHumiditySensor humiditySensor = new HTTPHumiditySensor(endpoint);
            result = humiditySensor.value();
        } catch (Exception e) {
            // Si on n'arrive pas a joindre l'url, on reset le tout, et on affiche que
            // le serveur n'est pas joignable pour cette url.
            stopTimer();
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    progressBar.setProgress(0);
                    responseHumidity.setText(R.string.starting_value_textview);
                    stopButton.setEnabled(false);
                    startButton.setEnabled(true);
                    Toast.makeText(
                            MainActivity.this,
                            "URL non joignable -> " + DEFAULT_ENDPOINT,
                            Toast.LENGTH_LONG
                    ).show();
                }
            });
        }
        Log.d(TAG, "Result: " + result);
        return result;
    }

}